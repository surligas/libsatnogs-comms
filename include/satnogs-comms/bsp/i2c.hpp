/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifndef I2C_HPP
#define I2C_HPP

#include <cstddef>
#include <cstdint>

namespace satnogs::comms::bsp
{
class i2c
{
public:
  i2c() {}
  ~i2c() {}

  /**
   * @brief Performs an I2C read operation
   *
   * @param addr address of the I2C device
   * @param rx the receive buffer. Should be at least \p rxlen bytes in size
   * @param tx the transmit buffer. Should be at least \p txlen bytes in size
   * @param rxlen number of bytes to received after the write operation
   * @param txlen number of bytes to transmit from the \p tx buffer
   */
  virtual void
  read(uint16_t addr, uint8_t *rx, size_t rxlen, const uint8_t *tx, size_t txlen) = 0;

  /**
   * @brief Simplified I2C read operation
   *
   * @param addr address of the I2C device
   * @param start_addr the peripheral internal address to start reading
   * @param rx the receive buffer. Should be at least \p len bytes in size
   * @param len number of bytes to receive
   */
  virtual void
  read(uint16_t addr, uint8_t start_addr, uint8_t *rx, size_t len) = 0;

  /**
   * @brief Performs an I2C write operation
   *
   * @param addr address of the I2C device
   * @param tx the transmit buffer. Should be at least \p len bytes in size
   * @param len number of bytes to be transmitted
   */
  virtual void
  write(uint16_t addr, const uint8_t *tx, size_t len) = 0;

  /**
   * @brief Performs an I2C write operation
   *
   * @param addr address of the I2C device
   * @param start_addr the peripheral internal address to start writing
   * @param tx the transmit buffer. Should be at least \p len bytes in size
   * @param len number of bytes to be transmitted
   */
  virtual void
  write(uint16_t addr, uint8_t start_addr, const uint8_t *tx, size_t len) = 0;

};

} // namespace satnogs::comms::bsp

#endif // I2C_HPP
