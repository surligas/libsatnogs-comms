/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#pragma once

#include <satnogs-comms/bsp/adc.hpp>
#include <satnogs-comms/bsp/gpio.hpp>
#include <satnogs-comms/bsp/i2c.hpp>
#include <satnogs-comms/emc1702.hpp>
#include <satnogs-comms/error.hpp>
#include <satnogs-comms/exception.hpp>
#include <satnogs-comms/ina322x.hpp>
#include <satnogs-comms/version.hpp>

namespace satnogs::comms
{

class power
{
public:
  static constexpr size_t IMOV_3V3_D_ADC_CH   = 16;
  static constexpr size_t IMOV_5V_RF_ADC_CH   = 0;
  static constexpr size_t IMOV_5V_FPGA_ADC_CH = 1;

  enum class subsys : uint8_t
  {
    CAN1      = 0,
    CAN2      = 1,
    RF_5V     = 2,
    FPGA_5V   = 3,
    CAN1_LPWR = 4,
    CAN2_LPWR = 5,
    UHF       = 6,
    SBAND     = 7
  };

  enum class sensor : uint8_t
  {
    EFUSES  = 0,
    EMC1702 = 1,
    AVERAGE = 2,
  };

  enum class channel : uint8_t
  {
    FPGA    = 1,
    RF_5V   = 2,
    DIG_3V3 = 3,
    VIN     = 4
  };

  /**
   * @brief Power good indicator
   *
   */
  enum class pgood_tp : uint8_t
  {
    RAIL_5V,
    RAIL_FPGA,
    RAIL_UHF,
    RAIL_SBAND
  };

  class r_lim
  {
  public:
    uint16_t dig_3v3;
    uint16_t rf_5v;
    uint16_t fpga_5v;

    r_lim() : dig_3v3(5600U), rf_5v(3600U), fpga_5v(5600U) {}
    r_lim(uint16_t dig_3v3, uint16_t rf_5v, uint16_t fpga_5v)
        : dig_3v3(dig_3v3), rf_5v(rf_5v), fpga_5v(fpga_5v)
    {
    }
  };

  class io_conf
  {
  public:
    bsp::i2c  &mon_i2c;
    bsp::gpio &rf_5v_en;
    bsp::gpio &fpga_5v_en;
    bsp::gpio &can1_en;
    bsp::gpio &can1_low_pwr;
    bsp::gpio &can2_en;
    bsp::gpio &can2_low_pwr;
    bsp::gpio &rf_5v_pgood;
    bsp::gpio &fpga_5v_pgood;
    bsp::gpio &uhf_en;
    bsp::gpio &uhf_pgood;
    bsp::gpio &sband_en;
    bsp::gpio &sband_pgood;
    bsp::adc  &imon_3v3_d;
    bsp::adc  &imon_5v_rf;
    bsp::adc  &imon_fpga;
    r_lim      rlim;
    uint16_t   efuse_adc_current_gain;
  };

  void
  enable(subsys sys, bool en = true);

  bool
  enabled(subsys sys) const;

  bool
  pgood(pgood_tp tp) const;

  float
  voltage(channel sys) const;

  float
  current(channel sys) const;

  float
  get_power(sensor src) const;

  power(const io_conf &io);

private:
  bsp::gpio &m_rf_5v_en;
  bsp::gpio &m_fpga_5v_en;
  bsp::gpio &m_can1_en;
  bsp::gpio &m_can1_low_pwr;
  bsp::gpio &m_can2_en;
  bsp::gpio &m_can2_low_pwr;
  bsp::gpio &m_pgood_5v;
  bsp::gpio &m_pgood_fpga;
  emc1702    m_monitor;
  bsp::gpio &m_uhf_en;
  bsp::gpio &m_uhf_pgood;
  bsp::gpio &m_sband_en;
  bsp::gpio &m_sband_pgood;
  bsp::adc  &m_imon_5v_rf;
  bsp::adc  &m_imon_3v3;
  bsp::adc  &m_imon_fpga;
  r_lim      m_r_lim;
  uint16_t   m_efuse_adc_current_gain;
};

} // namespace satnogs::comms
