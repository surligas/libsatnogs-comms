/*
 *  SatNOGS-COMMS control library
 *
 *  Copyright (C) 2022-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#include <satnogs-comms/error.hpp>
#include <satnogs-comms/rf_frontend09.hpp>

namespace satnogs::comms
{

rf_frontend09::rf_frontend09(io_conf &io, power &pwr)
    : rf_frontend({390e6, 500e6}, {390e6, 500e6}, std::move(io), pwr)
{
}

/**
 * Enables the basic subsystems of the RF frontend. Subsystems that are
 * used for a specific direction (RX or TX) are explicitly disabled to reduce
 * power consumption.
 *
 * Users may have to call the set_direction() method to enable the corresponding
 * subsystems such as PA or LNA.
 *
 * @param set true to enable, false to disable.
 */
void
rf_frontend09::enable(bool set)
{
  m_pwr.enable(power::subsys::UHF, set);
}

bool
rf_frontend09::enabled() const
{
  return m_pwr.enabled(power::subsys::UHF);
}

} // namespace satnogs::comms
