/*
 *  SatNOGS-COMMS MCU software
 *
 *  Copyright (C) 2024, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

/* UART exposed at the PC104 connector */
&usart3 {
    dmas = <&dmamux1 0 46 STM32_DMA_PERIPH_TX>,
           <&dmamux1 1 45 STM32_DMA_PERIPH_RX>;
    dma-names = "tx", "rx";
    pinctrl-0 = <&usart3_tx_pc10 &usart3_rx_pc11>;
    pinctrl-names = "default";
    current-speed = <115200>;
    status = "okay";
};
