###############################################################################
#  SatNOGS-COMMS MCU software
#
#  Copyright (C) 2022, Libre Space Foundation <http://libre.space>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#  SPDX-License-Identifier: GNU General Public License v3.0 or later
###############################################################################

if BOARD_SATNOGS_COMMS

config CAN1_ISOTP_TX_ADDR
    hex "ISOTP identifier for the CAN1 TX interface"
    default 0x1
    help
       This is the address that will be used for ISOTP frames
       that are transmitted from the CAN1 interface

config CAN1_ISOTP_RX_ADDR
    hex "ISOTP identifier for the CAN1 RX interface"
    default 0x2
    help
       This is the address that will be used for ISOTP FC frames
       that are transmitted from the CAN1 interface when ISOTP frames
       are received

config CAN1_ISOTP_REMOTE_PEER_TX_ADDR
    hex "ISOTP remote identifier used for TX frames for the CAN1"
    default 0x11

config CAN1_ISOTP_REMOTE_PEER_RX_ADDR
    hex "ISOTP identifier for the CAN1 RX interface"
    default 0x12

config MAX_MTU
    int "Maximum MTU size of all communication interfaces (CAN-ISOTP, RF, SPI, UART)"
    default 2048

config RX_MSGQ_SIZE
    int "The number of messages the RX message queue can hold"
    default 4
    help
       All communication interfaces submit their messages to this queue.
       Consider your workload when assigning a value.

config RADIO_RX_MSGQ_SIZE
    int "The number of messages the Radio RX message queue can hold"
    default 4
    help
       Both UHF and S-Band radio interfaces submit the decoded frame
       at the same message queue. This number defines the number of
       outstanding messages that can be pushed to the queue

config RADIO_TX_MSGQ_SIZE
    int "The number of messages the Radio TX message queue can hold"
    default 4
    help
       There is a single TX message queue for both UHF and S-Band radio
       interfaces. Having more than one it does not have any performance
       gain, since there is single SPI interface that communicates with
       the AT86RF215. This number defines the number of
       outstanding messages that can be pushed to the queue until the
       radio task issues them at the appropriate interface

config CAN1_TX_MSGQ_SIZE
    int "The number of messages the CAN1 message queue can hold"
    default 2

config MSG_ARBITER_STACK_SIZE
    int "Stack size of the message arbiter thread"
    default 2048
    help
      This thread handles quite complex routines with deep call stack.
      Consider a high stack size value

config MSG_ARBITER_PRIO
    int "Priority of the message arbiter thread"
    default 5

config CAN1_THREAD_STACK_SIZE
    int "Stack size of the CAN1 thread"
    default 1024

config CAN1_THREAD_PRIO
    int "Priority of the CAN1 thread"
    default 4

config RADIO_RX_THREAD_STACK_SIZE
    int "Stack size of the Radio RX thread"
    default 2048

config RADIO_RX_THREAD_PRIO
    int "Priority of the Radio RX thread"
    default 7

config RADIO_TX_THREAD_STACK_SIZE
    int "Stack size of the Radio TX thread"
    default 2048

config RADIO_TX_THREAD_PRIO
    int "Priority of the Radio TX thread"
    default 8

config MAIN_WORKQUEUE_STACK_SIZE
    int "Stack size of the main workqueue thread"
    default 512

config MAIN_WORKQUEUE_PRIO
    int "Priority of the main workqueue thread"
    default 5

config ASYNC_WORKQUEUE_STACK_SIZE
    int "Stack size of the workqueue thread used for async work items submission"
    default 2048

config ASYNC_WORKQUEUE_PRIO
    int "Priority of the main workqueue thread used for async work items submission"
    default 6

config TELEMETRY_THREAD_STACK_SIZE
    int "Stack size of the telemetry thread"
    default 2048

config TELEMETRY_THREAD_PRIO
    int "Priority of of the telemetry thread"
    default 3

config ENABLE_I2C_REVOVERY
    bool "Enable the I2C bus recovery mechanism"
    default y

config I2C_RECOVERY_RETRIES
    int "How many times to execute the bus recovery mechanism, until an error is raised"
    default 3
    range 1 1024

config ENABLE_DEBUG_UTILS
    bool "Enable extra debug utilities (e.g SeggerRTT)"
    default n

config OTA_SESSION_TIMEOUT_SECS
    int "The timout of the OTA session in seconds"
    default 259200
    help
      Due to scheduling and available passes over one or more ground stations,
      a succesfull OTA may take days to complete.
      Consider this when you set this value. The default is 3 days.

config R_LIM_3V3_D
    int "The resistor connected between the ILM pin and GND eFuse TPS25947xx at 3V3_D in Ohm"
    default 5600

config R_LIM_5V_FPGA
    int "The resistor connected between the ILM pin and GND eFuse TPS25947xx at 5V_FPGA in Ohm"
    default 3600

config R_LIM_5V_RF
    int "The resistor connected between the ILM pin and GND eFuse TPS25947xx at 5V_RF in Ohm"
    default 5600

config EFUSE_ADC_CURRENT_GAIN
    int "The ADC gain of the eFuse TPS25947xx"
    default 182


config WATCHDOG_PERIOD_SYS
    int "The watchdog period of the main loop in milliseconds"
    default 24000

config WATCHDOG_PERIOD_MSG_ARBITER
    int "The watchdog period of the message arbiter thread in milliseconds"
    default 24000

config WATCHDOG_PERIOD_TELEMETRY
    int "The watchdog period of the telemetry thread in milliseconds"
    default 24000

config WATCHDOG_PERIOD_RADIO_TX
    int "The watchdog period of the Radio TX thread in milliseconds"
    default 24000

config WATCHDOG_PERIOD_RADIO_RX
    int "The watchdog period of the Radio RX thread in milliseconds"
    default 24000

config WATCHDOG_PERIOD_CAN1
    int "The watchdog period of the CAN1 thread in milliseconds"
    default 24000

config DEPLOY_HOLD_OFF_SECS
    int "Deployement hold of period in seconds"
    default 2100

config TLM_PERIOD_SECS
    int "The period of the periodic telemety in seconds"
    default 30

config UHF_TX_FREQ_HZ
    int "The UHF TX frequency in Hz"

config UHF_RX_FREQ_HZ
    int "The UHF RX frequency in Hz"

config SBAND_TX_FREQ_HZ
    int "The S-Band TX frequency in Hz"

config SBAND_RX_FREQ_HZ
    int "The S-Band RX frequency in Hz"

config ERASE_SETTINGS
    bool "Set to true to erase all settings stored in the settings storage"
    default n

config TLC_SHA256_KEY
    string "Set the SHA256 of the uplink telecommands"
    default "00b0a7c2f63c426c59a10569183cab47ebb58a617d99ae6aa56f89fdd257b5c7"
    help
       This configuration accepts the SHA256 hash of the key that is used to athenticate
       the uplink telecommands. The hash should be given in hex form containing 64 characters.
       You can use directly the output of the sha256sum command

config PC104_UART_BAUDRATE
    int "The baudrate of the PC104 UART"
    default 115200

config PC104_UART_RX_TIMEOUT_MS
    int "The RX timeout of the PC104 UART RX"
    default 100
    help
      This is the timeout that use from the async UART API.
      The countdown starts from the last received character.

config UHF_TX_GAIN
    int "The TX gain for the UHF interface"
    default 12

config UHF_TX_FRAME_LEN
    int "The UHF TX frame len"
    default 448

config UHF_RX_FRAME_LEN
    int "The UHF RX frame len"
    default 256
    
config SBAND_TX_FRAME_LEN
    int "The SBAND TX frame len"
    default 448

config SBAND_RX_FRAME_LEN
    int "The SBAND RX frame len"
    default 256
endif